// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCFk_G9cpt52_RH-AsTXcNmZgWW66pvdbo",
    authDomain: "login-20302.firebaseapp.com",
    projectId: "login-20302",
    storageBucket: "login-20302.appspot.com",
    messagingSenderId: "882230357794",
    appId: "1:882230357794:web:539a45be9097baa695bf7b"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
