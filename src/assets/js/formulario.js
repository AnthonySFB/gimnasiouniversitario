var currentDateTime = new Date();
var year = currentDateTime.getFullYear();
var month = (currentDateTime.getMonth() + 1);
var date = (currentDateTime.getDate() + 1);

if(date < 10) {
  date = '0' + date;
}
if(month < 10) {
  month = '0' + month;
}

var dateTomorrow = year + "-" + month + "-" + date;
var checkinElem = document.querySelector("#checkindate");
var checkoutElem = document.querySelector("#checkoutdate");

checkinElem.setAttribute("min", dateTomorrow);

checkinElem.onchange = function () {
    checkoutElem.setAttribute("min", this.value);
}

window.onmousedown = function (e) {
    var el = e.target;
    if (el.tagName.toLowerCase() == 'option' && el.parentNode.hasAttribute('multiple')) {
        e.preventDefault();
  
        // toggle selection
        if (el.hasAttribute('selected')) el.removeAttribute('selected');
        else el.setAttribute('selected', '');
  
        // hack to correct buggy behavior
        var select = el.parentNode.cloneNode(true);
        el.parentNode.parentNode.replaceChild(select, el.parentNode);
    }
}



function sololetras(e){
    key=e.keyCode || e.which;
    teclado =String.fromCharCode(key).toLowerCase();
    letras=" aábcdeéfghiíjklmnñoópqrstuúvwxyz";
    especiales="8-37-38-46-164";
    teclado_especial=false;
    for( var i in especiales){  
    if(key==especiales[i]){
        teclado_especial=true;break;
    }
    }
    if(letras.indexOf(teclado)==-1 && !teclado_especial){
        return false;
    }
}

function solonumeros(e){
    key=e.keyCode || e.which;
    teclado =String.fromCharCode(key).toLowerCase();
    numero="0123456789";
    especiales="8-37-38-46";
    teclado_especial=false;
    for (var i in especiales){
    if(key==especiales[i]){
        teclado_especial=true;
    }
    if (numero.indexOf(teclado) ==-1 && !teclado_especial){
        return false;
    }
    }
}

var formulariLisit = [];

function addFomularioToSystem(name,cedu,email,phone,checkindate,checkoutdate){

    var newFomulario = {

        name: name,
        cedula: cedu,
        email: email,
        phone: phone,
        finicio: checkindate,
        ffinal: checkoutdate
    };

    console.log(newFomulario);
    formulariLisit.push(newFomulario);
    localStorageFormularioList(formulariLisit);

}


function getFormularioList(){
    var storedList = localStorage.getItem('localFormularioList');
    if (storedList == null){
        formulariLisit =  []
    } else {
        formulariLisit = JSON.parse(storedList);
    }
    return formulariLisit;
}

function localStorageFormularioList(plist){
    localStorage.setItem('localFormularioList', JSON.stringify((plist)));
}