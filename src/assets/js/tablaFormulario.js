document.querySelector('#btnAdquirir').addEventListener('click', saveFomulario);
drawFomularioTable();

function saveFomulario(){
    var sname = document.querySelector('#name').value,
        scedula = document.querySelector('#cedu').value,
        semail = document.querySelector('#email').value,
        sphone = document.querySelector('#phone').value,
        sfinicio = document.querySelector('#checkindate').value,
        sffinal = document.querySelector('#checkoutdate').value;

        addFomularioToSystem(sname,scedula,semail,sphone,sfinicio,sffinal);
        drawFomularioTable();
    
}

function drawFomularioTable(){
    var list = getFormularioList(),
        tbody = document.querySelector('#formularioTable tbody');

    tbody.innerHTML = '';

    for(var i = 0; i < list.length; i++){
        var row = tbody.insertRow(i),
            nameCell = row.insertCell(0),
            ceduCell = row.insertCell(1),
            emailCell = row.insertCell(2),
            phoneCell = row.insertCell(3),
            finicioCell = row.insertCell(4),
            ffinalCell = row.insertCell(5),
            selectCell = row.insertCell(6);

        nameCell.innerHTML = list[i].name;
        ceduCell.innerHTML = list[i].cedula;
        emailCell.innerHTML = list[i].email;
        phoneCell.innerHTML = list[i].phone;
        finicioCell.innerHTML = list[i].finicio;
        ffinalCell.innerHTML = list[i].ffinal;
        
        var inputSelect = document.createElement('input');
        inputSelect.type = 'radio';
        inputSelect.value = list[i].cedula;
        selectCell.appendChild(inputSelect);

        tbody.appendChild(row);
    }
    
}