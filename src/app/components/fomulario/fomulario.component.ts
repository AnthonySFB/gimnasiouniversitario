import { Component } from '@angular/core';
import { CargarScriptsService } from 'src/app/cargar-scripts.service';

@Component({
  selector: 'app-fomulario',
  templateUrl: './fomulario.component.html',
  styleUrls: ['./fomulario.component.css']
})
export class FomularioComponent {

  constructor(
    _CargaScripts:CargarScriptsService
  ){
    _CargaScripts.Carga(["formulario"])
  }

}
