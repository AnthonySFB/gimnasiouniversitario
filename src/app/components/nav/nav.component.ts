import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Router } from '@angular/router';
import { CargarScriptsService } from 'src/app/cargar-scripts.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
dataUser: any;

  constructor( 
    _CargaScripts:CargarScriptsService, 
    private afAuth: AngularFireAuth,
    private router: Router 
  ) {
    _CargaScripts.Carga(["script"]);
  }

  ngOnInit(): void {
    this.afAuth.currentUser.then(user => {
      if(user && user.emailVerified){
        this.dataUser = user;
      } else {
        this.router.navigate(['/login']);
      }
    })
  }

  logOut(){
    this.afAuth.signOut().then(() => this.router.navigate(['/login']));
  }
}
