import { Component } from '@angular/core';
import { CargarScriptsService } from 'src/app/cargar-scripts.service';

@Component({
  selector: 'app-testimony',
  templateUrl: './testimony.component.html',
  styleUrls: ['./testimony.component.css']
})
export class TestimonyComponent {

  constructor( 
    _CargaScripts:CargarScriptsService
  ) {
    _CargaScripts.Carga(["slider"]);
  }
}
