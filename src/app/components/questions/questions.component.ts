import { Component } from '@angular/core';
import { CargarScriptsService } from 'src/app/cargar-scripts.service';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.css']
})
export class QuestionsComponent {

  constructor( 
    _CargaScripts:CargarScriptsService
  ) {
    _CargaScripts.Carga(["questions"]);
  }

}
