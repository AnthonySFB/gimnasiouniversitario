import { Component } from '@angular/core';
import { CargarScriptsService } from 'src/app/cargar-scripts.service';

@Component({
  selector: 'app-tabla-fomulario',
  templateUrl: './tabla-fomulario.component.html',
  styleUrls: ['./tabla-fomulario.component.css']
})
export class TablaFomularioComponent {

  constructor( 
    _CargaScripts:CargarScriptsService
  ) {
    _CargaScripts.Carga(["tablaFormulario"]);
  }
}
