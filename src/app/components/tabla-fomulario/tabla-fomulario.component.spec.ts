import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaFomularioComponent } from './tabla-fomulario.component';

describe('TablaFomularioComponent', () => {
  let component: TablaFomularioComponent;
  let fixture: ComponentFixture<TablaFomularioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TablaFomularioComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TablaFomularioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
