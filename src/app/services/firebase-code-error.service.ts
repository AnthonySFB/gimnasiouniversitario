import { Injectable } from '@angular/core';
import { FirebaseCodeErrorEnum } from '../utils/firebase-code-error';

@Injectable({
  providedIn: 'root'
})
export class FirebaseCodeErrorService {

  constructor() { }

  codeError(code: string){
    switch(code){
      //El correo ya existe
      case FirebaseCodeErrorEnum.EmailAlreadyInUse:
        return 'El usuario ya existe';
      //La contrasena es debil
      case FirebaseCodeErrorEnum.WeakPassword:
        return 'La contraseña es muy debil';
      //Correo invalido
      case FirebaseCodeErrorEnum.InvalidEmail:
        return 'Correo invalido';
      //Contrasena incorrecta
      case FirebaseCodeErrorEnum.WrongPassword:
        return 'La contraseña es incorrecta';
      //Contrasena no existe
      case FirebaseCodeErrorEnum.UserNoFound:
        return 'El usuario es incorrecto o no existe';
      default:
        return 'Error deconocido';
    }
  }
}
