import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

//Servicios
import { CargarScriptsService } from './cargar-scripts.service';

//Modulos
import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularFireModule } from '@angular/fire/compat';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';


//Componentes
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { RegistrarUsuarioComponent } from './components/registrar-usuario/registrar-usuario.component';
import { VerificarCorreoComponent } from './components/verificar-correo/verificar-correo.component';
import { RecuperarPasswordComponent } from './components/recuperar-password/recuperar-password.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { environment } from 'src/environments/environment.prod';
import { BlogComponent } from './components/blog/blog.component';
import { UsuarioComponent } from './components/usuario/usuario.component';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { ScheduleComponent } from './components/schedule/schedule.component';
import { TestimonyComponent } from './components/testimony/testimony.component';
import { NavComponent } from './components/nav/nav.component';
import { KnowledgeComponent } from './components/knowledge/knowledge.component';
import { BlogsComponent } from './components/blogs/blogs.component';
import { FooterComponent } from './components/footer/footer.component';
import { AcercadeComponent } from './components/acercade/acercade.component';
import { ContactoComponent } from './components/contacto/contacto.component';
import { InicioPrincipalComponent } from './components/inicio-principal/inicio-principal.component';
import { PlanesComponent } from './components/planes/planes.component';
import { PlaneComponent } from './components/plane/plane.component';
import { FomularioComponent } from './components/fomulario/fomulario.component';
import { QuestionsComponent } from './components/questions/questions.component';
import { TablaFomularioComponent } from './components/tabla-fomulario/tabla-fomulario.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    InicioComponent,
    RegistrarUsuarioComponent,
    VerificarCorreoComponent,
    RecuperarPasswordComponent,
    SpinnerComponent,
    BlogComponent,
    UsuarioComponent,
    HomeComponent,
    AboutComponent,
    ScheduleComponent,
    TestimonyComponent,
    NavComponent,
    KnowledgeComponent,
    BlogsComponent,
    FooterComponent,
    AcercadeComponent,
    ContactoComponent,
    InicioPrincipalComponent,
    PlanesComponent,
    PlaneComponent,
    FomularioComponent,
    QuestionsComponent,
    TablaFomularioComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(), // ToastrModule added
  ],
  providers: [CargarScriptsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
