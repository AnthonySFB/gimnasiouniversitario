import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './components/about/about.component';
import { AcercadeComponent } from './components/acercade/acercade.component';
import { BlogComponent } from './components/blog/blog.component';
import { BlogsComponent } from './components/blogs/blogs.component';
import { ContactoComponent } from './components/contacto/contacto.component';
import { FomularioComponent } from './components/fomulario/fomulario.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { InicioPrincipalComponent } from './components/inicio-principal/inicio-principal.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { KnowledgeComponent } from './components/knowledge/knowledge.component';
import { LoginComponent } from './components/login/login.component';
import { NavComponent } from './components/nav/nav.component';
import { PlaneComponent } from './components/plane/plane.component';
import { PlanesComponent } from './components/planes/planes.component';
import { QuestionsComponent } from './components/questions/questions.component';
import { RecuperarPasswordComponent } from './components/recuperar-password/recuperar-password.component';
import { RegistrarUsuarioComponent } from './components/registrar-usuario/registrar-usuario.component';
import { ScheduleComponent } from './components/schedule/schedule.component';
import { TablaFomularioComponent } from './components/tabla-fomulario/tabla-fomulario.component';
import { TestimonyComponent } from './components/testimony/testimony.component';
import { VerificarCorreoComponent } from './components/verificar-correo/verificar-correo.component';

const routes: Routes = [
  { path: '', redirectTo: 'inicio-principal', pathMatch: 'full'},
  { path: 'inicio-principal', component: InicioPrincipalComponent},
  { path: 'login', component: LoginComponent},
  { path: 'registrar-usuario', component: RegistrarUsuarioComponent},
  { path: 'verificar-correo', component: VerificarCorreoComponent},
  { path: 'recuperar-password', component: RecuperarPasswordComponent},
  { path: 'inicio', component: InicioComponent},
  { path: 'nav', component: NavComponent},
  { path: 'home', component: HomeComponent},
  { path: 'about', component: AboutComponent},
  { path: 'knowledge', component: KnowledgeComponent},
  { path: 'schedule', component: ScheduleComponent},
  { path: 'planes', component: PlanesComponent},
  { path: 'testimony', component: TestimonyComponent},
  { path: 'questions', component: QuestionsComponent},
  { path: 'fomulario', component: FomularioComponent},
  { path: 'tabla-fomulario', component: TablaFomularioComponent},
  { path: 'blogs', component: BlogsComponent},
  { path: 'footer', component: FooterComponent},
  { path: 'acercade', component: AcercadeComponent},
  { path: 'contacto', component: ContactoComponent},
  { path: 'blog', component: BlogComponent},
  { path: 'plane', component: PlaneComponent},
  { path: '**', redirectTo: 'inicio-principal', pathMatch: 'full'},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
