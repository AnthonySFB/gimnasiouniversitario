export enum FirebaseCodeErrorEnum {
    EmailAlreadyInUse = 'auth/email-already-in-use',
    WeakPassword = 'auth/weak-password',
    InvalidEmail = 'auth/invalid-email',
    WrongPassword = 'auth/wrong-password',
    UserNoFound = 'auth/user-not-found',
}